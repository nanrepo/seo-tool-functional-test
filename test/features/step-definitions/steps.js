const wdio = require("webdriverio");
const assert = require("assert");
const { Given, When, And,Then } = require('cucumber');



Given(/^Estoy en la pantalla de login$/, () => {
    //driver.activateApp(("com.searchmas.es","com.searchmas.es"));
    driver.launchApp()
    driver.isAppInstalled(("com.searchmas.es","com.searchmas.es")), () => {
        console.log("La aplicación esta instalada correctamente");
    };
    var status = driver.queryAppState(("com.searchmas.es","com.searchmas.es"));
    switch (status) {
        case 0:
            console.log("is not installed");
          break;
        case 1:
            console.log("is not running");
          break;
        case 2:
            console.log("is running in background or suspended.");
          break;
          case 3:
          console.log("is running in background");
        break; 
        case 4:
            console.log("is running in foreground");
          break; 
      }
      expect($('~LoginScreenVIEW')).toBeExisting();
});

When(/^Selecciono el boton Continuar con un Search y Me logeo con las credenciales (.*) and (.*)$/, (username, password) => {
    $('button[type="submit" name="LoginScreen_boton"]').click(); 
    $('#username').setValue(username);
    $('#password').setValue(password);
    $('button[type="submit"]').click(); 
});
/*
And(/^Me logeo con las credenciales (.*) and (.*)$/, (username, password) => {
    $('#username').setValue(username);
    $('#password').setValue(password);
    $('button[type="submit"]').click(); 
});
**/

Then(/^Se visualiza el mensaje (.*)$/, (message) => {
    expect($('#flash')).toBeExisting();
    expect($('#flash')).toHaveTextContaining(message);
    afterEach(async function() {
        //await client.deleteSession();
    });});








