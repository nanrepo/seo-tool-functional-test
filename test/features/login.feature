Feature: LOGIN - SEARCHMAS

  Scenario Outline: Validar el login con credenciales no validas de un usuario de searchmas

    Given Estoy en la pantalla de login
    When Selecciono el boton Continuar con un Search+ y Me logeo con las credenciales <username> and <password>
    Then Se visualiza el mensaje <message>

    Examples:
      | username                             | password   | message                          |
      | gaston.genaud@searchmas.es           | gaston2020 | Error de logeo intente nuevamente|
