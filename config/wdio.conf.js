exports.config = {

    runner: 'local',
    specs: [
        './test/features/**/*.feature'
    ],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 10,
    capabilities: [{
        appWaitForLaunch: true,
        path: "/wd/hub",
        browserName: '',
        appiumVersion: '1.18.3',
        platformName: 'Android',
        platformVersion: '11',
        deviceName: 'Pixel_3a_Edited_API_30',
        app: "D:/SNAPSHOTS/S4/seo-tool/node_modules/seo-tool-functional-test/test/resources/mas-61beef52c94841a1a4669c8658b6f4e6-signed.apk", //adaptar con process.cwd() - Gaston Genaud
        automationName: 'UiAutomator2'
     }],
    logLevel: 'trace',
    bail: 0,
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ['appium'],
    port: 4723,
    path: "/wd/hub",
    framework: 'cucumber',
    reporters: ['spec'],
    cucumberOpts: {
        require: ['./test/features/step-definitions/steps.js'],
        backtrace: false,
        requireModule: [],
        dryRun: false,
        failFast: false,
        format: ['pretty'],
        snippets: true,
        source: true,
        profile: [],
        strict: false,
        tagExpression: '',
        timeout: 120000,
        ignoreUndefinedDefinitions: false
    },
    onPrepare: function () {
        console.log('<<< NATIVE APP TESTS STARTED >>>');
    },


    onComplete: function () {
        console.log('<<< TESTING FINISHED >>>');
    }
}
