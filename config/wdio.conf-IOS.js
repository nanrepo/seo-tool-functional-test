exports.config = {

    runner: 'local',
    specs: [
        './test/features/**/*.feature'
    ],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 10,
    capabilities: [{
        appWaitForLaunch: true,
        path: "/wd/hub",
        appiumVersion: '1.18.3',
        waitforTimeout,
        commandTimeout,
        browserName: 'iOS',
        platformName: 'iOS',
        unicodeKeyboard: true,
        newCommandTimeout: 30 * 60000,
        resetKeyboard: true,
        noReset: true,
        nativeInstrumentsLib: true,
        isolateSimDevice: true,
        platformVersion: "14.0.1",
        deviceName: "iPhone 7",
        app: "FALTA BUILD DE IOS",
        automationName: 'UiAutomator2'
     }],
    logLevel: 'trace',
    bail: 0,
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ['appium'],
    port: 4723,
    path: "/wd/hub",
    framework: 'cucumber',
    reporters: ['spec'],
    cucumberOpts: {
        require: ['./test/features/step-definitions/steps.js'],
        backtrace: false,
        requireModule: [],
        dryRun: false,
        failFast: false,
        format: ['pretty'],
        snippets: true,
        source: true,
        profile: [],
        strict: false,
        tagExpression: '',
        timeout: 120000,
        ignoreUndefinedDefinitions: false
    },
    onPrepare: function () {
        console.log('<<< NATIVE APP TESTS STARTED >>>');
    },


    onComplete: function () {
        console.log('<<< TESTING FINISHED >>>');
    }
}
