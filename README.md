![NaN](https://nantech.com.ar/wp-content/uploads/2020/03/logo.png)
# seo-tool-functional-test

_Herramientas para desarrollo de tests funcionales en apps NaN / nodejs_

## Comenzando 🚀

_Esta tool provee una implementación estándar de pruebas funcionales, orientada al testeo e2e de las screens de la app mobileSearchMas. 

Mira **Deployment** para conocer como desplegar el proyecto.

## Stack

`@wdio/cucumber-framework` para dar soporte a Gherkin
`mocha` para ejecución y redacción de tests
`wdio` para la ejecución  y configuración de entornos
`appium` para iniciar pruebas y generar enviroment dentro de IOS y Android
`eslint` para corrección y estructuración de codigo

## Estructura de carpetas

``` text
config/
    wdio.conf-IOS.js => Configuración de enviroment para IOS
    wdio.conf.js => Configuración de enviroment para ANDROID
test/
  resources/
    searchmas.apk
  features/
      loginScreen.feature
      singUpScreen.feature
    step-definitions/
      steps.js
```
Por convención, se cargan los pasos (GIVEN,WHEN,THEN) dentro de el archivo steps.js:

## config/wdio-conf.js

Utilizar para ejecutar tests programáticamente en un dispositivo real o un emulador de ANDROID modificando el nombre del dispositivo:

``` json
 platformName: 'Android',
        platformVersion: '11', => versión de android
        deviceName: 'Pixel_3a_Edited_API_30', => nombre del equipo (utilizar el comando "adb divices" para identificar el nombre)
        automationName: 'UiAutomator2'
```
## Configuración en package json

``` json
{
  "scripts": {
    "seo-tool-functional-test": "seo-tool-functional-test"
  }
}
```
# Copyright

Copyright (C) SearchMas. Todos los derechos reservados.